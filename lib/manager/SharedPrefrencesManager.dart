import 'package:shared_preferences/shared_preferences.dart';

const String KEY_WALLET = "wallet";

String wallet = "";


Future<void> loadPrefrences() async {
  var shared = await SharedPreferences.getInstance();
  
  if (shared.containsKey(KEY_WALLET)) {
    setPrefrence(shared.get(KEY_WALLET), KEY_WALLET);
  }
}

Future save<T>(T value, String key) async {
  var shared = await SharedPreferences.getInstance();
  if (value is String) {
    shared.setString(key, value);
  } else if (value is bool) {
    shared.setBool(key, value);
  }
  setPrefrence(value, key);
}

void setPrefrence(dynamic value, String key) {
  switch (key) {
    case KEY_WALLET:
      wallet = value;
      break;
    default:
  }
}

