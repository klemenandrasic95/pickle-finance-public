library router_manager;

import 'package:flutter/material.dart';

BuildContext mainContext;

void push(BuildContext context, dynamic controller) {
  var route = new MaterialPageRoute(
      builder: (BuildContext context) {
        return controller;
      }
  );
  Navigator.of(context).push(route);
}

void pushReplacement(BuildContext context, dynamic controller) {
  var route = new MaterialPageRoute(
      builder: (BuildContext context) {
        return controller;
      }
  );
  Navigator.of(context).pushReplacement(route);
}

Future<dynamic> pushForResult(BuildContext context, dynamic controller) async {
  var route = new MaterialPageRoute(
      builder: (BuildContext context) {
        return controller;
      }
  );
  return await Navigator.push(context, route);
}

void pushWithRouteName(BuildContext context, String name) {
  Navigator.pushNamed(context, name);
}

void pop(BuildContext context, {dynamic result}) {
  if(result != null) Navigator.pop(context, result);
  else Navigator.pop(context);
}

void popUntil(BuildContext context, String name) {
  Navigator.popUntil(context, ModalRoute.withName(name));
  // Navigator.of(context).popUntil(ModalRoute.withName(name));
}

void pushFromMain(dynamic controller) {
  var route = new MaterialPageRoute(
      builder: (BuildContext context) {
        return controller;
      }
  );
  Navigator.of(mainContext).push(route);
}

void popFromMain() {
  Navigator.of(mainContext).pop();
}
