import 'package:flutter/material.dart';
import 'package:pickle_finance/widgets/DonePopup.dart';

OverlayEntry _doneEntry;


void showDonePopup({@required BuildContext context, @required Function onDone}) {
  if (context != null) {
    if (_doneEntry != null) {
      closeDone();
    }
    _doneEntry = OverlayEntry(builder: (BuildContext context) {
      return Positioned(
        bottom: MediaQuery.of(context).viewInsets.bottom,
        left: 0,
        right: 0,
        child: DonePopup(onDone: onDone)
      );
    });
    Navigator.of(context).overlay.insert(_doneEntry);
  }
}

void closeDone() {
  if (_doneEntry != null) {
    _doneEntry.remove();
    _doneEntry = null;
  }
}