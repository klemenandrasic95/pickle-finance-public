import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {

  bool isYearMonthDayEqual(DateTime date) {
    return this.year == date.year && this.month == date.month && this.day == date.day ? true : false;
  }

  bool isEqual(DateTime date) {
    return this.year == date.year && this.month == date.month && this.day == date.day && this.hour == date.hour && this.minute == date.minute ? true : false;
  }

  bool isDifferent(DateTime date) {
    if (this == null && date != null) {return true;}
    else if (this == null && date == null) {return false;}
    else if (this != null && date == null) {return true;}
    return !this.isEqual(date);
  }

  bool isDifferentDMY(DateTime date) {
    if (this == null && date != null) {return true;}
    else if (this == null && date == null) {return false;}
    else if (this != null && date == null) {return true;}
    return !this.isYearMonthDayEqual(date);
  }

  String monthName() {
    return DateFormat("MMMM").format(this);
  }

  String toReadableDateDMY() {
    return DateFormat("d. M. y").format(this);
  }

  String toReadableTimeHM() {
    return DateFormat("HH:mm").format(this);
  }

  String toReadableDateTime() {
    return DateFormat("d. M. y - HH:mm").format(this);
  }

  String toBackendFormat() {
    return DateFormat("yyyy-MM-dd").format(this);
  }

  String toReadableTimeHms() {
    return DateFormat("HH:mm:ss").format(this);
  }

  DateTime joinTime(DateTime time) {
    return DateTime(this.year, this.month, this.day, time.hour, time.minute);
  }
}

extension StringExtension on String {
  String toWalletAddress() {
    if (this.length > 10) {
      String _new = "";
      _new += this.substring(0, 5);
      _new += "...";
      _new += this.substring(this.length - 4, this.length);
      return _new;
    } else {
      return "No wallet connected";
    }
  }
}
