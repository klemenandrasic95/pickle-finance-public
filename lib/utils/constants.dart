import 'package:flutter/material.dart';

const Color color_background = Color(0xFF0E1D15);
const Color color_background2 = Color(0xFF1f1f1f);
const Color color_red = Color(0xFFFF2626);
const Color color_text = Color(0xFF26FF91);
const Color color_text2 = Color(0xFFEBEBEB);
const Color color_link = Color(0xFF93FCE2);