import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pickle_finance/dataModels/PickleChart.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:provider/provider.dart';
import 'package:pickle_finance/utils/extensions.dart';


//Variables

const main_url = "http://api.coingecko.com/api/v3";
BuildContext _context;

//Setters

void setAPIContext(BuildContext context) {
  _context = context;
}

//Helpers

String combine(String url) => main_url + url;

//CoinGecko calls to API

Future getPicklePrice(String to) async {
  var dio = Dio();
  var res = await dio.get(combine("/simple/price?ids=pickle-finance&vs_currencies=$to"));
  var json = res.data;
  Provider.of<PickleStore>(_context, listen: false).setPicklePrice(json["pickle-finance"]["usd"]);
}

Future getOtherPrices() async {
  var dio = Dio();
  dio.get(combine("/simple/price?ids=ethereum&vs_currencies=usd")).then((value) => {
    Provider.of<PickleStore>(_context, listen: false).setEthPrice(value.data["ethereum"]["usd"])
  });
  dio.get(combine("/simple/price?ids=dai&vs_currencies=usd")).then((value) => {
    Provider.of<PickleStore>(_context, listen: false).setDaiPrice(value.data["dai"]["usd"])
  });
  dio.get(combine("/simple/price?ids=tether&vs_currencies=usd")).then((value) => {
    Provider.of<PickleStore>(_context, listen: false).setUsdtPrice(value.data["tether"]["usd"])
  });
  dio.get(combine("/simple/price?ids=usd-coin&vs_currencies=usd")).then((value) => {
    Provider.of<PickleStore>(_context, listen: false).setUsdcPrice(value.data["usd-coin"]["usd"])
  });
}

Future getPickleData() async {
  var dio = Dio();
  var res = await dio.get(combine("/coins/pickle-finance"));
}

Future getPickleChart(int days) async {
  var dio = Dio();
  var res = await dio.get(combine("/coins/pickle-finance/market_chart?vs_currency=usd&days=$days"));
  var json = res.data;

  List<PickleChart> _chart = List<PickleChart>();
  (json["prices"] as List).forEach((element) {
    _chart.add(PickleChart(DateTime.fromMillisecondsSinceEpoch(element[0]), element[1]));
  });
  Provider.of<PickleStore>(_context, listen: false).setChart(_chart);
}