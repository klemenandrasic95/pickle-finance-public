
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pickle_finance/manager/SharedPrefrencesManager.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:provider/provider.dart';

const String _key = "HGFIHDDV85P9W6FNYUSMHMEPESUJKRAYBR";

BuildContext _context;

//Setters

void setEtherAPIContext(BuildContext context) {
  _context = context;
}

//Etherscan.io API calls

Future getContractData() async {
  var dio = Dio();
  var res = await dio.get("https://api.etherscan.io/api?module=stats&action=tokensupply&contractaddress=0x429881672B9AE42b8EbA0E26cD9C73711b891Ca5&apikey=$_key");
  
  var cap = res.data["result"].toString();
  var finalCap = int.parse(cap.substring(0, cap.length - 18));

  Provider.of<PickleStore>(_context, listen: false).setTotalSupply(finalCap);
}

Future getUserPickleBalance() async {
  var dio = Dio();
  if (wallet == "") {
    return;
  }
  var res = await dio.get("https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0x429881672B9AE42b8EbA0E26cD9C73711b891Ca5&address=$wallet&tag=latest&apikey=$_key");

  var pickles = res.data["result"].toString();
  var finalPickles = double.parse(pickles.substring(0, pickles.length - 18));

  var decimal = pickles.substring(pickles.length - 18, pickles.length);
  var finalDecimal = "0." + decimal.substring(0, 3);
  var finalFinalPickles = finalPickles + double.parse(finalDecimal);

  Provider.of<PickleStore>(_context, listen: false).setUserPickles(finalFinalPickles);
}