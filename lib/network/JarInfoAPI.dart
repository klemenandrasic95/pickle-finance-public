import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:provider/provider.dart';

BuildContext _context;

//Setters

void setJarInfoAPIContext(BuildContext context) {
  _context = context;
}

Future getPoolsData() async {
  var dio = Dio();
  var res = await dio.get("https://api.pickle-jar.info/protocol/value");
  Provider.of<PickleStore>(_context, listen: false).setJarValues(res.data);
}