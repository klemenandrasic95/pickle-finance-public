class Pickle {
  String contractAddress;
  int marketCapRank;
  int marketCap;
  int totalVolume;
  DateTime lastUpdate;

  //24
  int priceChange24;


  Pickle.fromJSON(Map<String, dynamic> map) {
    contractAddress = map["contract_address"];
    marketCapRank = map["market_cap_rank"];
    marketCap = map["market_cap"]["usd"];
    totalVolume = map["total_volume"]["usd"];
    priceChange24 = map["price_change_24h_in_currency"]["usd"];
    lastUpdate = DateTime.parse(map["last_updated"]);
  }
}