import 'package:flutter/material.dart';
import 'package:pickle_finance/dataModels/PickleChart.dart';

class PickleStore extends ChangeNotifier {
  double _userPickles = 0.0;
  double get userPickles => _userPickles;

  double get userPicklesInDollar => _userPickles * _picklePrice;

  double _picklePrice = 0.0;
  double get picklePrice => _picklePrice;

  double _ethPrice = 0.0;
  double get ethPrice => _ethPrice;

  double _daiPrice = 0.0;
  double get daiPrice => _daiPrice;

  double _usdcPrice = 0.0;
  double get usdcPrice => _usdcPrice;

  double _usdtPrice = 0.0;
  double get usdtPrice => _usdtPrice;

  List<PickleChart> _chart = List<PickleChart>();
  List<PickleChart> get chart => _chart;

  DateTime _lastUpdate = DateTime.now();
  DateTime get lastUpdate => _lastUpdate;

  int _totalSupply = 0;
  int get totalSupply => _totalSupply;

  double get marketCap => _totalSupply * _picklePrice;

  int _selectedGraphIndex = 0;
  int get selectedGraphIndex => _selectedGraphIndex;
  int get daysToGet {
    if (_selectedGraphIndex == 0) return 1;
    else if (_selectedGraphIndex == 1) return 3;
    else if (_selectedGraphIndex == 2) return 7;
    else if (_selectedGraphIndex == 3) return 30;
    else if (_selectedGraphIndex == 4) return 365;
    else return 1;
  }

  String _wallet = "";
  String get wallet => _wallet;


  //Jars

  double _jarScrv = 0.0;
  double get jarScrv => _jarScrv;

  double _jarWbtcEth = 0.0;
  double get jarWbtcEth => _jarWbtcEth;

  double _jarDaiEth = 0.0;
  double get jarDaiEth => _jarDaiEth;

  double _jarUsdcEth = 0.0;
  double get jarUsdcEth => _jarUsdcEth;

  double _jarUsdtEth = 0.0;
  double get jarUsdtEth => _jarUsdtEth;

  double _jarCdai = 0.0;
  double get jarCdai => _jarCdai;

  double _jar3PoolCrv = 0.0;
  double get jar3PoolCrv => _jar3PoolCrv;

  double _jarRenBtcCrv = 0.0;
  double get jarRenBtcCrv => _jarRenBtcCrv;

  double _jarTotalValue = 0.0;
  double get jarTotalValue => _jarTotalValue;

  void setJarValues(Map<String, dynamic> map) {
    _jarWbtcEth = double.parse(map["wbtc-eth"].toString());
    _jarDaiEth = double.parse(map["dai-eth"].toString());
    _jarUsdcEth = double.parse(map["usdc-eth"].toString());
    _jarUsdtEth = double.parse(map["usdt-eth"].toString());
    _jarCdai = double.parse(map["cdai"].toString());
    _jar3PoolCrv = double.parse(map["3poolcrv"].toString());
    _jarScrv = double.parse(map["scrv"].toString());
    _jarRenBtcCrv = double.parse(map["renbtccrv"].toString());
    _jarTotalValue = double.parse(map["totalValue"].toString());
    notifyListeners();
  }

  void setUserPickles(double value) {
    _userPickles = value;
    notifyListeners();
  }

  void setWallet(String value) {
    _wallet = value;
    notifyListeners();
  }

  void setSelectedGraphIndex(int value) {
    _selectedGraphIndex = value;
    notifyListeners();
  }

  void setTotalSupply(int value) {
    _totalSupply = value;
    notifyListeners();
  }

  void setLastUpdate(DateTime value) {
    _lastUpdate = value;
    notifyListeners();
  }
  
  void setPicklePrice(double value) {
    _picklePrice = value;
    notifyListeners();
  }

  void setDaiPrice(double value) {
    _daiPrice = value;
    notifyListeners();
  }

  void setEthPrice(double value) {
    _ethPrice = value;
    notifyListeners();
  }

  void setUsdcPrice(double value) {
    _usdcPrice = value;
    notifyListeners();
  }

  void setUsdtPrice(double value) {
    _usdtPrice = value;
    notifyListeners();
  }

  void setChart(List<PickleChart> value) {
    _chart = value;
    notifyListeners();
  } 
}
