import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pickle_finance/manager/RouterManager.dart';
import 'package:pickle_finance/screens/MainScreen.dart';
import 'package:pickle_finance/utils/constants.dart';

class InitScreen extends StatefulWidget {
  @override
  _InitScreenState createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {


  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(Duration(seconds: 2), () {
        push(context, MainScreen());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color_background,
      body: Container(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(child: Container()),
              Container(
                child: Text("The future of finance is green", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 32, shadows: [Shadow(color: color_text, blurRadius: 10)]), textAlign: TextAlign.center),
              ),
              Container(
                margin: EdgeInsets.only(top: 24),
                child: Image.asset("assets/icons/pickle.png")
              ),
              Expanded(child: Container()),
            ],
          ),
        ),
      ),
    );
  }
}
