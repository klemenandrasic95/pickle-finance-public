import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pickle_finance/manager/SharedPrefrencesManager.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:pickle_finance/utils/constants.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Future.delayed(Duration(milliseconds: 500), () {
        setState(() {
          _textEditingController.text = wallet;
        });
        Provider.of<PickleStore>(context, listen: false).setWallet(wallet);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text("My account", style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 22)),
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 16),
                  padding: EdgeInsets.all(14),
                  decoration: BoxDecoration(
                    border: Border.all(color: color_text, width: 1.5),
                    color: color_background2,
                    borderRadius: BorderRadius.circular(8)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("My wallet", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 20)),
                      Container(
                        margin: EdgeInsets.only(top: 12),
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          border: Border.all(color: color_text),
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child: TextField(
                          controller: _textEditingController,
                          onChanged: (text) {
                            save(text, KEY_WALLET);
                            Provider.of<PickleStore>(context, listen: false).setWallet(text);
                          },
                          onSubmitted: (text) {
                            save(text, KEY_WALLET);
                            Provider.of<PickleStore>(context, listen: false).setWallet(text);
                          },
                          keyboardType: TextInputType.text,
                          cursorColor: color_text,
                          decoration: InputDecoration.collapsed(hintText: "Your ETH wallet", hintStyle: GoogleFonts.sourceCodePro(color: color_text2.withOpacity(0.5), fontSize: 22)),
                          style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 22),
                        )
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: Container(),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 16),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      child: Text("Data provided by: ", style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 16)),
                    ),
                    Container(
                      child: Text("Coingecko", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 16)),
                    )
                  ],
                ),
                Container(
                  child: Text("Made with 💚 by Klemen Andraisc (ShadoPanda)", style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 16)),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}