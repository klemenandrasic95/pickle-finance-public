import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pickle_finance/manager/SharedPrefrencesManager.dart';
import 'package:pickle_finance/screens/Calculator/CalculatorScreen.dart';
import 'package:pickle_finance/screens/Jars/JarsScreen.dart';
import 'package:pickle_finance/screens/Settings/SettingsScreen.dart';
import 'package:pickle_finance/screens/Stats/StatsScreen.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:pickle_finance/utils/constants.dart';
import 'package:provider/provider.dart';
import 'package:pickle_finance/utils/extensions.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;
  
  @override
  void initState() {
    super.initState();

    //Load prefrences if there are any
    loadPrefrences();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: color_background,
      bottomNavigationBar: Container(
        height: 90,
        decoration: BoxDecoration(
          color: color_background2,
          boxShadow: [
            BoxShadow(color: color_background2, blurRadius: 10, offset: Offset(0, -1))
          ]
        ),
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom, left: 20, right: 20, top: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _selectedIndex = 0;
                  });
                },
                child: Container(
                  color: Colors.transparent,
                  child: SvgPicture.asset("assets/icons/dashboard.svg", width: 25, height: 25, color: _selectedIndex == 0 ? color_text : color_text2),
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _selectedIndex = 1;
                  });
                },
                child: Container(
                  color: Colors.transparent,
                  child: SvgPicture.asset("assets/icons/jar.svg", width: 25, height: 25, color: _selectedIndex == 1 ? color_text : color_text2),
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _selectedIndex = 2;
                  });
                },
                child: Container(
                  color: Colors.transparent,
                  child: SvgPicture.asset("assets/icons/calculator.svg", width: 25, height: 25, color: _selectedIndex == 2 ? color_text : color_text2),
                ),
              ),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _selectedIndex = 3;
                  });
                },
                child: Container(
                  color: Colors.transparent,
                  child: SvgPicture.asset("assets/icons/settings.svg", width: 25, height: 25, color: _selectedIndex == 3 ? color_text : color_text2),
                ),
              ),
            )
          ],
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        child: Column(
          children: [
            Consumer<PickleStore>(
              builder: (context, store, child) {
                return Container(
                  padding: EdgeInsets.only(right: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _selectedIndex = 3;
                          });
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: store.wallet.length < 10 ? color_red : color_text,
                                borderRadius: BorderRadius.circular(40),
                                boxShadow: [
                                  BoxShadow(color: store.wallet.length < 10 ? color_red : color_text, blurRadius: 10.0)
                                ]
                              ),
                              height: 15,
                              width: 15,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 8),
                              child: Text(store.wallet.toWalletAddress(), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w500, fontSize: 16))
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 8, bottom: 16),
                        child: Text("Last updated: ${store.lastUpdate.toReadableTimeHms()}", style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w500, fontSize: 16)),
                      )
                    ],
                  ),
                );
              },
            ),
            Expanded(
              child: IndexedStack(
                index: _selectedIndex,
                children: [
                  StatsScreen(),
                  JarsScreen(),
                  CalculatorScreen(),
                  SettingsScreen()
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}