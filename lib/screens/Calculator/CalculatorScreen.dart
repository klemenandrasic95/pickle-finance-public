import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pickle_finance/manager/PopupManager.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:pickle_finance/utils/constants.dart';
import 'package:provider/provider.dart';

class CalculatorScreen extends StatefulWidget {
  @override
  _CalculatorScreenState createState() => _CalculatorScreenState();
}

class _CalculatorScreenState extends State<CalculatorScreen> {
  TextEditingController _controller = TextEditingController();

  int _selectedValueType = 0;
  double _calculatedPickles = 0.0;

  void _calculate() {
    double price = Provider.of<PickleStore>(context, listen: false).picklePrice;
    double ethPrice = Provider.of<PickleStore>(context, listen: false).ethPrice;

    double userInputPrice = 0.0;
    if (_selectedValueType == 1) {
      userInputPrice = double.parse(_controller.text);
    } else {
      userInputPrice = double.parse(_controller.text) * ethPrice;
    }
    
    setState(() {
      _calculatedPickles = (userInputPrice / price);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Text("Value calculator", style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 22)),
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(top: 16),
                    padding: EdgeInsets.all(14),
                    decoration: BoxDecoration(
                      border: Border.all(color: color_text, width: 1.5),
                      color: color_background2,
                      borderRadius: BorderRadius.circular(8)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selectedValueType = 0;
                                  _calculate();
                                });
                              },
                              child: Container(
                                child: Text("ETH", style: GoogleFonts.sourceCodePro(color: _selectedValueType == 0 ? color_text : color_text2, fontSize: 24)),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  _selectedValueType = 1;
                                  _calculate();
                                });
                              },
                              child: Container(
                                margin: EdgeInsets.only(left: 12, right: 12),
                                child: Text("USD", style: GoogleFonts.sourceCodePro(color: _selectedValueType == 1 ? color_text : color_text2, fontSize: 22)),
                              ),
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: color_text),
                                  borderRadius: BorderRadius.circular(8)
                                ),
                                child: TextField(
                                  controller: _controller,
                                  onTap: () {
                                    showDonePopup(context: context, onDone: () {
                                      closeDone();
                                      FocusScope.of(context).unfocus();
                                    });
                                  },
                                  onChanged: (text) {
                                    _calculate();
                                  },
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  cursorColor: color_text,
                                  decoration: InputDecoration.collapsed(hintText: "0.000", hintStyle: GoogleFonts.sourceCodePro(color: color_text2.withOpacity(0.5), fontSize: 22)),
                                  style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 22),
                                )
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16, bottom: 16),
                          height: 1,
                          color: color_text,
                        ),
                        Container(
                          child: Text("You will get", style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 16)),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Row(
                            children: [
                              Container(
                                child: Text("~${_calculatedPickles.toStringAsFixed(3)}", style: GoogleFonts.sourceCodePro(color: color_text, fontWeight: FontWeight.w600, fontSize: 26))
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 8),
                                child: Image.asset("assets/icons/pickle_icon.png", width: 28, height: 28),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}