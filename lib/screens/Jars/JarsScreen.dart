import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:pickle_finance/utils/constants.dart';
import 'package:provider/provider.dart';

class JarsScreen extends StatefulWidget {
  @override
  _JarsScreenState createState() => _JarsScreenState();
}

class _JarsScreenState extends State<JarsScreen> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 16),
      color: color_background,
      child: Consumer<PickleStore>(
        builder: (context, store, child) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text("Jars", style: GoogleFonts.sourceCodePro(color: color_text2, fontSize: 24)),
                ),
                Container(
                  margin: EdgeInsets.only(top: 16),
                  height: 380,
                  decoration: BoxDecoration(
                    border: Border.all(color: color_text, width: 1.5),
                    color: color_background2,
                    borderRadius: BorderRadius.circular(8)
                  ),
                  child: Column(
                    children: [
                      Expanded(
                        child: PageView(
                          onPageChanged: (index) {
                            setState(() {
                              _selectedIndex = index;
                            });
                          },
                          children: [
                            Container(
                              padding: EdgeInsets.all(14),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("pJar 0", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 24, shadows: [Shadow(color: color_text, blurRadius: 10)])),
                                  Container(
                                    margin: EdgeInsets.only(top: 8),
                                    child: Text("sCRV", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarScrv), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 12),
                                    child: Text("3poolCRV", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jar3PoolCrv), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 12),
                                    child: Text("renBTCCRV", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarRenBtcCrv), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(14),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("pJar 0.69", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 24, shadows: [Shadow(color: color_text, blurRadius: 10)])),
                                  Container(
                                    margin: EdgeInsets.only(top: 8),
                                    child: Text("DAI-ETH", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarDaiEth), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 12),
                                    child: Text("USDC-ETH", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarUsdcEth), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 12),
                                    child: Text("USDT-ETH", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarUsdtEth), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 12),
                                    child: Text("WBTC-ETH", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarWbtcEth), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(14),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("pJar 0.88", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 24, shadows: [Shadow(color: color_text, blurRadius: 10)])),
                                  Container(
                                    margin: EdgeInsets.only(top: 8),
                                    child: Text("cDAI", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 18)),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 2),
                                    child: Text(NumberFormat.currency(symbol: "\$").format(store.jarCdai), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: _selectedIndex == 0 ? color_text : color_text2,
                                borderRadius: BorderRadius.circular(40),
                                boxShadow: [
                                  BoxShadow(color: _selectedIndex == 0 ? color_text : color_text2, blurRadius: 10.0)
                                ]
                              ),
                              height: 10,
                              width: 10,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 12, right: 12),
                              decoration: BoxDecoration(
                                color: _selectedIndex == 1 ? color_text : color_text2,
                                borderRadius: BorderRadius.circular(40),
                                boxShadow: [
                                  BoxShadow(color: _selectedIndex == 1 ? color_text : color_text2, blurRadius: 10.0)
                                ]
                              ),
                              height: 10,
                              width: 10,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: _selectedIndex == 2 ? color_text : color_text2,
                                borderRadius: BorderRadius.circular(40),
                                boxShadow: [
                                  BoxShadow(color: _selectedIndex == 2 ? color_text : color_text2, blurRadius: 10.0)
                                ]
                              ),
                              height: 10,
                              width: 10,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(top: 24),
                        padding: EdgeInsets.only(bottom: 12),
                        decoration: BoxDecoration(
                          border: Border.all(color: color_text, width: 1.5),
                          color: color_background2,
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 14, top: 14, right: 14),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Total jar value", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 20)),
                                  Text(NumberFormat.currency(symbol: "\$").format(store.jarTotalValue), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}