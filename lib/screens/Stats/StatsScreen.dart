import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:pickle_finance/dataModels/PickleChart.dart';
import 'package:pickle_finance/network/CoinGeckoAPI.dart';
import 'package:pickle_finance/network/EtherscanAPI.dart';
import 'package:pickle_finance/network/JarInfoAPI.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:pickle_finance/utils/constants.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart';


class StatsScreen extends StatefulWidget {
  @override
  _StatsScreenState createState() => _StatsScreenState();
}

class _StatsScreenState extends State<StatsScreen> {

  Timer timer;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      //Just set a context that we can use always
      setAPIContext(context);
      setEtherAPIContext(context);
      setJarInfoAPIContext(context);
      _fetchData();
      _updateTimer();

      Future.delayed(Duration(milliseconds: 500), () {
        getUserPickleBalance();
      });
    });
  }

  static List<Series<PickleChart, DateTime>> _createSampleData(List<PickleChart> chartData) {
    return [
      Series<PickleChart, DateTime>(
        id: 'Price',
        colorFn: (_, __) => ColorUtil.fromDartColor(color_text),
        areaColorFn: (_, __) => ColorUtil.fromDartColor(color_text.withOpacity(0.2)),
        patternColorFn: (_, __) => ColorUtil.fromDartColor(color_text),
        domainFn: (PickleChart sales, _) => sales.dateTime,
        measureFn: (PickleChart sales, _) => sales.price,
        data: chartData,
      )
    ];
  }

  void _updateTimer() {
    if (timer != null) {
      timer.cancel();
    }
    timer = Timer.periodic(Duration(seconds: 30), (timer) {
      _fetchData();
      Provider.of<PickleStore>(context, listen: false).setLastUpdate(DateTime.now());
    });
  }

  //Fetch the data
  Future _fetchData() async {
    var store = Provider.of<PickleStore>(context, listen: false);
    getPicklePrice("usd");
    getPickleChart(store.daysToGet);
    getOtherPrices();
    getContractData();
    getPoolsData();
    getUserPickleBalance();
    Provider.of<PickleStore>(context, listen: false).setLastUpdate(DateTime.now());
  }


  Future _changePickleChartTo(int days, int index) async {
    Provider.of<PickleStore>(context, listen: false).setSelectedGraphIndex(index);
    _fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<PickleStore>(
        builder: (context, store, child) {
          return Container(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: RefreshIndicator(
                      strokeWidth: 2.0,
                      backgroundColor: Colors.transparent,
                      color: color_text,
                      onRefresh: () async {
                        _updateTimer();
                        _fetchData();
                      },
                      child: SingleChildScrollView(
                        physics: AlwaysScrollableScrollPhysics(),
                        child: Container(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 24),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: color_text, width: 1.5),
                                          color: color_background2,
                                          borderRadius: BorderRadius.circular(8)
                                        ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.only(left: 14, top: 14, right: 14),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text("Balance", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 20)),
                                                  Row(
                                                    children: [
                                                      Text(store.userPickles.toStringAsFixed(3), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                                      Container(
                                                        margin: EdgeInsets.only(left: 8),
                                                        child: Image.asset("assets/icons/pickle_icon.png", width: 28, height: 28),
                                                      )
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(left: 14, bottom: 8),
                                              child: Text(NumberFormat.currency(symbol: "\$").format(store.userPicklesInDollar), style: GoogleFonts.sourceCodePro(color: color_text2.withOpacity(0.5), fontWeight: FontWeight.w600, fontSize: 16))
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 24, bottom: 24),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(color: color_text, width: 1.5),
                                          color: color_background2,
                                          borderRadius: BorderRadius.circular(8)
                                        ),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(14),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text("Market cap", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 20)),
                                                  Text(NumberFormat.currency(symbol: "\$").format(store.marketCap), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              height: 1,
                                              color: color_text,
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(left: 14, top: 8, bottom: 8),
                                              child: Text("Total supply: " + NumberFormat.currency(symbol: "\$").format(store.totalSupply), style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 16))
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.all(14),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: color_text, width: 1.5),
                                        color: color_background2,
                                        borderRadius: BorderRadius.circular(8)
                                      ),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text("Prices", style: GoogleFonts.sourceCodePro(color: color_text, fontSize: 20)),
                                          PriceItem(name: "PICKLE", price: store.picklePrice.toString(), icon: "assets/icons/pickle_icon.png"),
                                          PriceItem(name: "ETH", price: store.ethPrice.toString(), icon: "assets/icons/eth_icon.png"),
                                          PriceItem(name: "DAI", price: store.daiPrice.toString(), icon: "assets/icons/dai_icon.png"),
                                          PriceItem(name: "USDC", price: store.usdcPrice.toString(), icon: "assets/icons/usdc_icon.png"),
                                          PriceItem(name: "USDT", price: store.usdtPrice.toString(), icon: "assets/icons/usdt_icon.png"),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 24),
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  border: Border.all(color: color_text, width: 1.5),
                                  color: color_background2,
                                  borderRadius: BorderRadius.circular(8)
                                ),
                                child: store.chart.isNotEmpty ? Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(12),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          GestureDetector(
                                            onTap: () => _changePickleChartTo(1, 0),
                                            child: Container(child: Text("1D", style: GoogleFonts.sourceCodePro(color: store.selectedGraphIndex == 0 ? color_text : color_text2, fontWeight: FontWeight.w500, fontSize: 18)))
                                          ),
                                          GestureDetector(
                                            onTap: () => _changePickleChartTo(3, 1),
                                            child: Container(child: Text("3D", style: GoogleFonts.sourceCodePro(color: store.selectedGraphIndex == 1 ? color_text : color_text2, fontWeight: FontWeight.w500, fontSize: 18)))
                                          ),
                                          GestureDetector(
                                            onTap: () => _changePickleChartTo(7, 2),
                                            child: Container(child: Text("7D", style: GoogleFonts.sourceCodePro(color: store.selectedGraphIndex == 2 ? color_text : color_text2, fontWeight: FontWeight.w500, fontSize: 18)))
                                          ),
                                          GestureDetector(
                                            onTap: () => _changePickleChartTo(30, 3),
                                            child: Container(child: Text("1M", style: GoogleFonts.sourceCodePro(color: store.selectedGraphIndex == 3 ? color_text : color_text2, fontWeight: FontWeight.w500, fontSize: 18)))
                                          ),
                                          GestureDetector(
                                            onTap: () => _changePickleChartTo(365, 4),
                                            child: Container(child: Text("1Y", style: GoogleFonts.sourceCodePro(color: store.selectedGraphIndex == 4 ? color_text : color_text2, fontWeight: FontWeight.w500, fontSize: 18)))
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      height: 250,
                                      child: TimeSeriesChart(
                                        _createSampleData(store.chart),
                                        animate: false,
                                        defaultRenderer: LineRendererConfig(stacked: true, includeArea: true),
                                        primaryMeasureAxis: NumericAxisSpec(
                                          renderSpec: GridlineRendererSpec(
                                            labelStyle: TextStyleSpec(
                                              color: ColorUtil.fromDartColor(color_text2)
                                            )
                                          ),
                                          tickProviderSpec: BasicNumericTickProviderSpec(zeroBound: false, dataIsInWholeNumbers: false, desiredTickCount: 1, desiredMaxTickCount: 1, desiredMinTickCount: 1)
                                        ),
                                        domainAxis: DateTimeAxisSpec(
                                          renderSpec: GridlineRendererSpec(
                                            lineStyle: LineStyleSpec(
                                              color: ColorUtil.fromDartColor(Colors.transparent)
                                            ),
                                            labelStyle: TextStyleSpec(
                                              color: ColorUtil.fromDartColor(color_text2)
                                            )
                                          ),
                                        ),
                                      )
                                    ),
                                  ],
                                ) : Container(),
                              ),
                              Container(
                                height: 100,
                                color: Colors.transparent
                              )
                            ]
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class PriceItem extends StatelessWidget {
  final String name;
  final String price;
  final String icon;

  PriceItem({Key key, this.name, this.price, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 8),
        child: Row(
          children: [
            Image.asset(icon, width: 28, height: 28),
            Container(
              margin: EdgeInsets.only(left: 8),
                child: Text(name, style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w300, fontSize: 24))
            ),
            Expanded(child: Container()),
            Text("\$" + price, style: GoogleFonts.sourceCodePro(color: color_text2, fontWeight: FontWeight.w600, fontSize: 24)),
          ],
        )
    );
  }
}
