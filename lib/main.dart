import 'package:flutter/material.dart';
import 'package:pickle_finance/screens/InitScreen.dart';
import 'package:pickle_finance/screens/MainScreen.dart';
import 'package:pickle_finance/store/PickleStore.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => PickleStore()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: InitScreen(),
    );
  }
}




