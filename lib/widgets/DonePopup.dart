import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pickle_finance/manager/PopupManager.dart';
import 'package:pickle_finance/utils/constants.dart';


class DonePopup extends StatefulWidget {
  final Function onDone;

  DonePopup({Key key, this.onDone}): super(key: key);

  @override
  _DonePopupState createState() => _DonePopupState();
}

class _DonePopupState extends State<DonePopup> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: color_background2,
      child: Align(
        alignment: Alignment.topRight,
        child: CupertinoButton(
          padding: EdgeInsets.only(right: 24.0, top: 8.0, bottom: 8.0),
          onPressed: () {
            if (widget.onDone != null) {
              widget.onDone();
            } else {
              closeDone();
            }
          },
          child: Text("Done", style: TextStyle(color: color_text, fontWeight: FontWeight.bold)
          ),
        ),
      ),
    );
  }
}